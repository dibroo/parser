﻿#load "./Parser.fsx"

open Syntax

let check (p : parser<_,_>) name word =
    let r = p 0 (word + "\n")
    match r with
    | Success _  -> printfn "%s pass %s" name word
    | Failure i -> printfn ">>>>>>>>>>>>>> %s fails %s : %d <<<<<<<<<<<<<<<" name word i

let fails (p : parser<_,_>) name word =
    let r = p 0 (word + "\n")
    match r with
    | Failure _  -> printfn "%s pass %s" name word
    | Success _ -> printfn ">>>>>>>>>>>>>> %s fails %s <<<<<<<<<<<<<<<" name word

let checkAll p name words = 
    words |> List.iter (check p name)

let failsAll p name words =
    words |> List.iter (fails p name)

let test p name w1 w2 =
    checkAll p name w1
    failsAll p name w2

let digitTests () =
    let w = [
        "1"
        "2"
        "3"
        "4"
        "5"
        "6."
    ]

    let f = [
        "a"; "b"; "c"; "!"; "+"; ""
    ]
    test DIGIT "DIGIT" w f
    
let twoDigigtsTests () =
    let w1 = ["11"; "21"; "222"; "34"]
    let w2 = ["1"; "a1"; "1a"; ""]
    test (DIGIT >. DIGIT) "Two Digits" w1 w2

let letterTests () =
    let w1 = ["w"; "ww"; "s "; "t"]
    let w2 = ["!"; "!a"; "1"; "-"]
    test LETTER "LETTER" w1 w2

let letterOrDigitTests () =
    let w1 = ["1"; "w"; "1w"; "w1"]
    let w2 = ["!"; "@"; " "; ""]
    test (DIGIT |. LETTER) "Letter or digit" w1 w2

let twoLetterOrTwoDigitsTests () =
    let w1 = ["aa"; "11"]
    let w2 = ["a1"; "1a"; "1"; "b"; "@@"]
    let p =  (LETTER >. LETTER) |. (DIGIT >. DIGIT)
    test p "Two Letters or Two Digits" w1 w2

let letterOrDigitAndLetterOrDigitTests () =
    let w1 = ["a1"; "1a"; "aa"; "11"]
    let w2 = ["a"; "!"; "1"; "@@"; "1!"; "b!"]
    let p = (LETTER |. DIGIT) >. (LETTER |. DIGIT)
    test p "(Letter or Digit) and (Letter or Digit)" w1 w2

let ATest () =
    let w1 = ["A"]
    let w2 = ["a"; "1"; ","]
    test ``A`` "A" w1 w2

let dotAfterDigitTests () =
    let w1 = ["1."; "9."]
    let w2 = ["Aa"; "11"; "1"]
    let p = DIGIT >? ``.``
    test p "DIGIT." w1 w2

let dotAfterDigitAndLetterTests () =
    let w1 = ["1.A"; "9.V"]
    let w2 = ["Aa"; "11"; "1"; "1.1"]
    let p = DIGIT >? ``.`` >. ``.`` >. LETTER
    test p "DIGIT.LETTER" w1 w2

let notLetterTests () =
    let w1 = ["w"; "ww"; "s "; "t"]
    let w2 = ["!"; "!a"; "1"; "-"]
    test (!! LETTER) "NOT LETTER" w2 w1

let letterAndOptDigitTest () =
    let w1 = ["a"; "a1"; "aa"]
    let w2 = ["1"]
    let p = LETTER >. (!? DIGIT)
    test p "LETTER DIGIT?" w1 w2

let anyCountLetterTest () =
    let w1 = [""; "a"; "ab"; "aabc"; "aaassdc"]
    let w2 = []
    test (!* LETTER) "LETTER*" w1 w2

let posCountLetterTest () =
    let w1 = ["a"; "ab"; "aabc"; "aaassdc"]
    let w2 = ["1"; ""]
    test (!+ LETTER) "LETTER+" w1 w2

let wordTest () =
    let w1 = ["word"]
    let w2 = ["a"; "wort"]
    test (WORD "word") "WORD" w1 w2

let epsTest () =
    let w1 = [""; "1"; "2"; "3"]
    let w2 = []
    test EPS "EPS" w1 w2 

let whiteSpaceTest () =
    let w1 = ["\n"; " "; "\r"; "\t"]
    let w2 = ["a"; "b"; "1"]
    test WHITESPACE "WHITESPACE" w1 w2

let anyTest () =
    let w1 = ["a"; "1"; "2"; "b"]
    let w2 = ["c"; "3"; "4"; "d"]
    test (any [``a``; ``1``; ``2``; ``b``]) "ANY" w1 w2


digitTests ()
twoDigigtsTests ()
letterTests ()
letterOrDigitTests ()
twoLetterOrTwoDigitsTests ()
letterOrDigitAndLetterOrDigitTests () 
ATest ()
dotAfterDigitTests ()
dotAfterDigitAndLetterTests ()
notLetterTests ()
letterAndOptDigitTest ()
anyCountLetterTest ()
posCountLetterTest ()
wordTest () 
epsTest ()
whiteSpaceTest ()
anyTest ()
