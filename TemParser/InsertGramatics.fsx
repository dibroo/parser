#r "System.dll"

open System
open System.IO
open System.Diagnostics

let lines = File.ReadAllText("./Parser.ttx")
let gramm = File.ReadAllText("./Grammatics.grm")
let r = lines.Replace("@$---@$", gramm)
File.WriteAllText("./Parser.tt", r)

let parserPath = Path.Combine(Directory.GetCurrentDirectory(), "parser.tt")

let p = new Process()
p.StartInfo.FileName <- "powershell.exe"
p.StartInfo.Arguments <- "-executionpolicy remotesigned -command TextTransform " + "\"" + parserPath + "\""
p.StartInfo.RedirectStandardInput <- true
p.StartInfo.UseShellExecute <- false
p.Start()