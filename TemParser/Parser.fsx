module Syntax
#r "System.dll"

// *************************************************************  
// ��������:
//   A >. B - A � B
//   A |. B - ���
//   A >? B - ���� ����� A ���� B, �� �
//   !! A   - �� A
//   !* A   - A*
//   !+ A   - A+
//   !? A   - A ����������� �� ����� ������ ����
//   ``a``  - ������

//  back i p    - ��������� �� i �������� ����� � ��������� p
//  next i p    - ������� �� i �������� ����� � ��������� p

//  WORD "�����"    - �����
//  EOF             - ����� ������
//  LETTER          - ����� �����
//  DIGIT           - ����� �����
//  EPS             - ������ ������
//  WHITESPACE      - ���������, ������, ������� ������� ��� ������� �� ����� ������


open System
open System.Collections.Generic

type Result<'succ, 'fail> =
    | Success of 'succ
    | Failure of 'fail

type ParsingResult<'token> =
    Result<'token * int, int> 

type Token =
    | Eof
    | Eps
    | List of Token * Token
    | Digit of char
    | Letter of char
    | Operator of char
    | Whitespace of char
    | Word of Token
    | Main of Token

type parser<'a, 'b> = int -> 'a -> ParsingResult<'b>

let resultHandler thenSucc thenFail (r : ParsingResult<_>) : ParsingResult<_> =
    match r with
    | Success a -> thenSucc a
    | Failure b -> thenFail b
    
let private fconst a _ = a

let EPS = fun i w -> Success(Eps, i)

let (>.) (a : parser<_, _>) (b : parser<_, _>) : parser<_,_> =
    fun i w -> 
        let thenSucc ((_, k) as r) =
            let append (a, i) (b, j) = Success(List(a, b), j) 
            resultHandler (append r) Failure (b k w)
        resultHandler thenSucc Failure (a i w)

let (|.) (a : parser<_, _>) (b : parser<_, _>) : parser<_,_> =
    fun i w ->
        let thenFail _ = b i w
        resultHandler Success thenFail (a i w)

let back j (a : parser<_,_>) : parser<_,_> =
    fun i w -> a (i - j) w 

let inline next j = back -j

let (>?) (a : parser<_,_>) (b : parser<_,_>) : parser<_,_> =
    fun i w -> 
        //let thenSucc ((_, k) as r) =
        //    resultHandler ((Success >> fconst) r) Failure (b k w)
        //resultHandler thenSucc Failure (a i w)
        let r = a i w 
        match r with
        | Success (l, j) ->
            let r2 = b j w
            match r2 with
            | Success _ -> r
            | x -> x
        | y -> y

let (!*) (a : parser<_,_>) : parser<_,_>  =
    let rec anyCount a i w =
        ((a >. (anyCount a)) |. EPS) i w
    anyCount a

let (!+) a = a >. (!*a)

let (!!) (a : parser<_,_>) : parser<_,_> = 
    fun i w -> resultHandler (snd >> Failure) (fun i -> Success(Eps, i)) (a i w)

let (!?) (a : parser<_,_>) : parser<_,_> =
    fun i w -> resultHandler Success (fun i -> Success(Eps, i))  (a i w)

let private wrap wrapper (n, i) = Success(wrapper n, i)

let EOF =
    fun i (w : string) ->
        if w.Length = i then
            Success(Eof, i)
        else
            Failure i

let private charToken pred token : parser<string,_> =
    (fun (i : int) (w : string) ->
        if pred w.[i] then
            Success(token w.[i], (i + 1))
        else Failure i)

let private ischar c tp = charToken ((=) c) tp
let private chars = new Dictionary<char, parser<string, Token>>()

let any (ps : _ list) =
    ps.Tail |> List.fold (|.) ps.Head

let DIGIT = charToken (Char.IsDigit) Digit
let LETTER = charToken (Char.IsLetter) Letter  

let ``a`` = ischar 'a' Letter
chars.['a'] <- ``a``
let ``b`` = ischar 'b' Letter
chars.['b'] <- ``b``
let ``c`` = ischar 'c' Letter
chars.['c'] <- ``c``
let ``d`` = ischar 'd' Letter
chars.['d'] <- ``d``
let ``e`` = ischar 'e' Letter
chars.['e'] <- ``e``
let ``f`` = ischar 'f' Letter
chars.['f'] <- ``f``
let ``g`` = ischar 'g' Letter
chars.['g'] <- ``g``
let ``h`` = ischar 'h' Letter
chars.['h'] <- ``h``
let ``i`` = ischar 'i' Letter
chars.['i'] <- ``i``
let ``j`` = ischar 'j' Letter
chars.['j'] <- ``j``
let ``k`` = ischar 'k' Letter
chars.['k'] <- ``k``
let ``l`` = ischar 'l' Letter
chars.['l'] <- ``l``
let ``m`` = ischar 'm' Letter
chars.['m'] <- ``m``
let ``n`` = ischar 'n' Letter
chars.['n'] <- ``n``
let ``o`` = ischar 'o' Letter
chars.['o'] <- ``o``
let ``p`` = ischar 'p' Letter
chars.['p'] <- ``p``
let ``q`` = ischar 'q' Letter
chars.['q'] <- ``q``
let ``r`` = ischar 'r' Letter
chars.['r'] <- ``r``
let ``s`` = ischar 's' Letter
chars.['s'] <- ``s``
let ``t`` = ischar 't' Letter
chars.['t'] <- ``t``
let ``u`` = ischar 'u' Letter
chars.['u'] <- ``u``
let ``v`` = ischar 'v' Letter
chars.['v'] <- ``v``
let ``w`` = ischar 'w' Letter
chars.['w'] <- ``w``
let ``x`` = ischar 'x' Letter
chars.['x'] <- ``x``
let ``y`` = ischar 'y' Letter
chars.['y'] <- ``y``
let ``z`` = ischar 'z' Letter
chars.['z'] <- ``z``
let ``A`` = ischar 'A' Letter
chars.['A'] <- ``A``
let ``B`` = ischar 'B' Letter
chars.['B'] <- ``B``
let ``C`` = ischar 'C' Letter
chars.['C'] <- ``C``
let ``D`` = ischar 'D' Letter
chars.['D'] <- ``D``
let ``E`` = ischar 'E' Letter
chars.['E'] <- ``E``
let ``F`` = ischar 'F' Letter
chars.['F'] <- ``F``
let ``G`` = ischar 'G' Letter
chars.['G'] <- ``G``
let ``H`` = ischar 'H' Letter
chars.['H'] <- ``H``
let ``I`` = ischar 'I' Letter
chars.['I'] <- ``I``
let ``J`` = ischar 'J' Letter
chars.['J'] <- ``J``
let ``K`` = ischar 'K' Letter
chars.['K'] <- ``K``
let ``L`` = ischar 'L' Letter
chars.['L'] <- ``L``
let ``M`` = ischar 'M' Letter
chars.['M'] <- ``M``
let ``N`` = ischar 'N' Letter
chars.['N'] <- ``N``
let ``O`` = ischar 'O' Letter
chars.['O'] <- ``O``
let ``P`` = ischar 'P' Letter
chars.['P'] <- ``P``
let ``Q`` = ischar 'Q' Letter
chars.['Q'] <- ``Q``
let ``R`` = ischar 'R' Letter
chars.['R'] <- ``R``
let ``S`` = ischar 'S' Letter
chars.['S'] <- ``S``
let ``T`` = ischar 'T' Letter
chars.['T'] <- ``T``
let ``U`` = ischar 'U' Letter
chars.['U'] <- ``U``
let ``V`` = ischar 'V' Letter
chars.['V'] <- ``V``
let ``W`` = ischar 'W' Letter
chars.['W'] <- ``W``
let ``X`` = ischar 'X' Letter
chars.['X'] <- ``X``
let ``Y`` = ischar 'Y' Letter
chars.['Y'] <- ``Y``
let ``Z`` = ischar 'Z' Letter
chars.['Z'] <- ``Z``
let ``0`` = ischar '0' Digit
chars.['0'] <- ``0``
let ``1`` = ischar '1' Digit
chars.['1'] <- ``1``
let ``2`` = ischar '2' Digit
chars.['2'] <- ``2``
let ``3`` = ischar '3' Digit
chars.['3'] <- ``3``
let ``4`` = ischar '4' Digit
chars.['4'] <- ``4``
let ``5`` = ischar '5' Digit
chars.['5'] <- ``5``
let ``6`` = ischar '6' Digit
chars.['6'] <- ``6``
let ``7`` = ischar '7' Digit
chars.['7'] <- ``7``
let ``8`` = ischar '8' Digit
chars.['8'] <- ``8``
let ``9`` = ischar '9' Digit
chars.['9'] <- ``9``
let ``!`` = ischar '!' Operator
chars.['!'] <- ``!``
let ``#`` = ischar '#' Operator
chars.['#'] <- ``#``
let ``$`` = ischar '$' Operator
chars.['$'] <- ``$``
let ``%`` = ischar '%' Operator
chars.['%'] <- ``%``
let ``'`` = ischar ''' Operator
chars.['''] <- ``'``
let ``(`` = ischar '(' Operator
chars.['('] <- ``(``
let ``)`` = ischar ')' Operator
chars.[')'] <- ``)``
let ``*`` = ischar '*' Operator
chars.['*'] <- ``*``
let ``+`` = ischar '+' Operator
chars.['+'] <- ``+``
let ``,`` = ischar ',' Operator
chars.[','] <- ``,``
let ``-`` = ischar '-' Operator
chars.['-'] <- ``-``
let ``.`` = ischar '.' Operator
chars.['.'] <- ``.``
let ``/`` = ischar '/' Operator
chars.['/'] <- ``/``
let ``:`` = ischar ':' Operator
chars.[':'] <- ``:``
let ``;`` = ischar ';' Operator
chars.[';'] <- ``;``
let ``<`` = ischar '<' Operator
chars.['<'] <- ``<``
let ``>`` = ischar '>' Operator
chars.['>'] <- ``>``
let ``=`` = ischar '=' Operator
chars.['='] <- ``=``
let ``?`` = ischar '?' Operator
chars.['?'] <- ``?``
let ``@`` = ischar '@' Operator
chars.['@'] <- ``@``
let ``[`` = ischar '[' Operator
chars.['['] <- ``[``
let ``]`` = ischar ']' Operator
chars.[']'] <- ``]``
let ``^`` = ischar '^' Operator
chars.['^'] <- ``^``
let ``_`` = ischar '_' Operator
chars.['_'] <- ``_``
let ``{`` = ischar '{' Operator
chars.['{'] <- ``{``
let ``}`` = ischar '}' Operator
chars.['}'] <- ``}``
let ``|`` = ischar '|' Operator
chars.['|'] <- ``|``
let ``~`` = ischar '~' Operator
chars.['~'] <- ``~``
let `` ` `` = ischar '`' Operator
chars.['`'] <- `` ` ``
let ``\`` = ischar '\\' Operator
chars.['\\'] <- ``\``
let ``"`` = ischar '"' Operator
chars.['"'] <- ``"``

let WORD (s : string) = 
    let fc = 
        s.ToCharArray() 
        |> Seq.map (fun c -> chars.[c]) 
        |> List.ofSeq
    fc.Tail |> List.fold (>.) fc.Head

let WHITESPACE =
    (ischar '\n' Whitespace)
    |.  (ischar ' ' Whitespace)
    |.  (ischar '\r' Whitespace)
    |.  (ischar '\t' Whitespace)

let rec MAIN : parser<string, Token> =    
    fun i w ->
        let r = (         DIGIT  ) i w
        resultHandler (wrap Main) Failure r

